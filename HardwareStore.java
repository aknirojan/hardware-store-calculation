package bcas.ap.uml.hardwarestore.exe;

public class HardwareStore {
	
	String PartNumber;
	String PartDescription;
	int QuantityItem;
	double PricePerItem;
	public char[] getInvoice;

	
	public HardwareStore(String part, String description, int quantity, double price ) {
		PartNumber = part;
		PartDescription = description;
		QuantityItem = quantity;
		PricePerItem = price;
		
	}


	public String getPartNumber() {
		return PartNumber;
	}


	public void setPartNumber(String partNumber) {
		PartNumber = partNumber;
	}


	public String getPartDescription() {
		return PartDescription;
	}


	public void setPartDescription(String partDescription) {
		PartDescription = partDescription;
	}


	public int getQuantityItem() {
		return QuantityItem;
	}


	public void setQuantityItem(int quantityItem) {
		QuantityItem = quantityItem;
	}


	public double getPricePerItem() {
		return PricePerItem;
	}


	public void setPricePerItem(double pricePerItem) {
		PricePerItem = pricePerItem;
	}
	public double getInvoice() {
		double TotalAmount = QuantityItem * PricePerItem;
		return TotalAmount;
	}
	
}
