package bcas.ap.uml.hardwarestore.exe;

import java.util.Scanner;

public class HardwareExecution {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		HardwareStore execution = new HardwareStore(null, null, 0, 0);

		System.out.println("Enter The Part Number:");
		execution.setPartNumber(scan.next());

		System.out.println("Enter The Part Description:");
		execution.setPartDescription(scan.next());

		System.out.println("Enter The Quantity Item:");
		execution.setQuantityItem(scan.nextInt());

		System.out.println("Enter The Part Number");
		execution.setPricePerItem(scan.nextInt());

		System.out.println(execution.getInvoice());

	}

}
